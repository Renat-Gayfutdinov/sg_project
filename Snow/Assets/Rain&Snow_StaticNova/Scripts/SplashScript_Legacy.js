var splash: Transform;

function LateUpdate () 
{
	var theParticles = GetComponent.<UnityEngine.Timeline.ParticleControlPlayable>().particles;
	
	for(var i = 0; i < GetComponent.<UnityEngine.Timeline.ParticleControlPlayable>().particleCount;i++)
	{
		if(theParticles[i].energy > GetComponent.<UnityEngine.Timeline.ParticleControlPlayable>().maxEnergy)
		{
			var splashObj: Transform = Transform.Instantiate(splash, theParticles[i].position, Quaternion.identity);
			theParticles[i].energy = 0;
			Destroy(splashObj.gameObject, 0.5);
		}
	}	
	GetComponent.<UnityEngine.Timeline.ParticleControlPlayable>().particles = theParticles;
}