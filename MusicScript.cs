﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour {

    public float bpm = 120;

    //the current position of the song (in seconds)
    public float songPosition;

    //the current position of the song (in beats)
    public float songPosInBeats;

    //the duration of a beat
    public float secPerBeat;

    //how much time (in seconds) has passed since the song started
    public float dsptimesong;

    public float nextBeatTime;

    public float beatDelta;
    public float beatDeltaAbs;

    // Use this for initialization
    void Start () {

        //calculate how many seconds is one beat
        //we will see the declaration of bpm later
        Application.targetFrameRate = 60;

        secPerBeat = 60f / bpm;     
        //record the time when the song starts
        dsptimesong = (float)AudioSettings.dspTime;

        nextBeatTime = 0;

        //start the song
        songPosition = (float)(AudioSettings.dspTime - dsptimesong);
        GetComponent<AudioSource>().Play();
    }
	
	// Update is called once per frame
	void Update () {
        print(Time.time);
        //calculate the position in seconds
        songPosition = (float)(AudioSettings.dspTime - dsptimesong);
        
        //calculate the position in beats
        songPosInBeats = songPosition / secPerBeat;

        //print(Mathf.Abs(nextBeatTime - songPosition));

        beatDelta = songPosition - nextBeatTime;
        beatDeltaAbs  = Mathf.Abs(nextBeatTime - songPosition);

        if (beatDelta < 0.07f && beatDelta >= 0f)
        {
            nextBeatTime += secPerBeat;   
        }
    }
}
