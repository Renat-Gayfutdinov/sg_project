﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoving : MonoBehaviour
{
    public GameObject MusicPlayer;

    public int hp;
    public int timingAccuracy;
    private int currentTimingLevel;
    public int currentTiming;

    public Transform map;
    public GameObject musicPlayer;

    delegate void DashState();
    DashState dashstate;

    Vector3 dashStart;
    Vector3 dashDestination;
    private float startTime;
    private float dashLength;
    private float dashSpeed;

    Vector3 jumpDestination;
    float jumpLength;
    float jumpSpeed;

    float delayTime;

    private int trackNumber;

    AudioSource audioSource;
    public AudioClip tapSound;

    // Use this for initialization
    void Start()
    {
        hp = 5;

        timingAccuracy = 0;
        dashstate = DashReady;
        dashSpeed = 30f;
        trackNumber = 0;
        jumpSpeed = 0;
        jumpDestination = transform.position + Vector3.up;
        jumpLength = Vector3.Distance(transform.position, jumpDestination);
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        dashstate();
    }

    void Dashing()
    {

        float distCovered = (Time.time - startTime) * dashSpeed;
        float fracDash = distCovered / dashLength;
        dashStart += Vector3.up * jumpSpeed;
        transform.position = Vector3.Lerp(dashStart, dashDestination, fracDash);
        if (transform.position == dashDestination)
        {
            jumpSpeed = 0f;
            dashstate = DashReady;
        }
    }

    void Delayed()
    {
        delayTime -= Time.deltaTime;
        if (delayTime < 0f)
        {
            startTime = Time.time;
            dashstate = Dashing;
        }

    }

    void DashReady()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            dashDestination = transform.position + transform.forward * map.lossyScale.z * 0.25f;
        }
        if (Input.GetKeyDown(KeyCode.A) && trackNumber > -1)
        {
            dashDestination = transform.position + (-transform.right + transform.forward) * map.lossyScale.z * 0.33f;
            trackNumber--;
        }
        if (Input.GetKeyDown(KeyCode.D) && trackNumber < 1)
        {
            dashDestination = transform.position + (transform.right + transform.forward) * map.lossyScale.z * 0.33f;
            trackNumber++;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpSpeed = 0.25f;
            dashDestination = transform.position + transform.forward * map.lossyScale.z * 0.25f;
        }

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Space))
        {
            CheckTiming(musicPlayer.GetComponent<MusicScript>().nextBeatTime, Time.time);
            audioSource.PlayOneShot(tapSound);
            delayTime = musicPlayer.GetComponent<MusicScript>().secPerBeat / 2f;
            dashStart = transform.position;
            dashLength = Vector3.Distance(transform.position, dashDestination);
            dashstate = Delayed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Barrier"))
        {
            TakeDamage();
        }
    }

    void CheckTiming(float beatTime, float tapTiming)
    {

        if (musicPlayer.GetComponent<MusicScript>().beatDeltaAbs < 0.300f)
        {
            timingAccuracy += 1;
            currentTiming = 1;
        } else
        {
            timingAccuracy -= 1;
            currentTiming = 0;
        }

        if (timingAccuracy > currentTimingLevel)
        {
            GetPrize();
            timingAccuracy = 0;
            currentTimingLevel += 10;
        }

        if (timingAccuracy < -5)
        {
            TakeDamage();
            timingAccuracy = 0;
        }
    }

    void TakeDamage()
    {
        if (hp > 1)
        {
            hp--;
        } else
        {
            Die();
        }
    }

    void GetPrize()
    {

    }
   
    void Die()
    {
        

    }
}