﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimingManager : MonoBehaviour {

    public float beatCenter;
    public GameObject menuManager;

    public bool currentTiming;
    public int timingFailure;
    public int timingSuccess;

    public float countDownTime = 0;
    bool ready = false;

    public GameObject musicPlayer;

    delegate void TimingState();
    TimingState timingState;

    public float timer;

    public UnityEvent FailureEvent;
    public UnityEvent SuccesEvent;

	// Use this for initialization
	void Start () {

        //menuManager.GetComponent<MenuManager>().pauseEvent.AddListener(Pause);
        //menuManager.GetComponent<MenuManager>().playEvent.AddListener(Play);

        if (FailureEvent == null)
        {
            FailureEvent = new UnityEvent();
        }
        if (SuccesEvent == null)
        {
            SuccesEvent = new UnityEvent();
        }
        musicPlayer.GetComponent<MusicScript>().timingEvent.AddListener(ChangeTimingState);        
        musicPlayer.GetComponent<MusicScript>().countDownEvent.AddListener(WaitForCountDownEnd);
        beatCenter = 0;
        timingState = WaitingForMissedTap;
	}
	
	// Update is called once per frame
	void Update ()
    {
        countDownTime -= Time.deltaTime;
        if (countDownTime <= 0)
        {
            ready = true;
        }
        if (ready)
        {
            ControlAccuracy();
            timingState();
        }
	}

    void Play ()
    {
        timer = musicPlayer.GetComponent<MusicScript>().timingDuration * 2;
        timingState = WaitingForMissedTap;
    }

    void Pause ()
    {
        timingState = Idle;
    }

    void Idle ()
    {

    }

    void ControlAccuracy()
    {
        if (timingFailure > 3)
        {
            timingFailure = 0;
            FailureEvent.Invoke();
        }
        if (timingSuccess > 30)
        {
            SuccesEvent.Invoke();
        }
    }

    void ChangeTimingState()
    {
        timer = musicPlayer.GetComponent<MusicScript>().timingDuration * 2;
        timingState = WaitingForTap;
        //print("start " + Time.time);
    }

    void WaitingForTap()
    {
        timer -= Time.deltaTime;

        if (timer >= 0)
        {
            //if (musicPlayer.GetComponent<MusicScript>().readyToPlay)
            //{
                if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                {
                    //print("good");
                    //print("tap " + Time.time);
                    //print("end " + (Time.time + timer));
                    timingSuccess += 1;
                    currentTiming = true;
                    timingState = WaitingForMissedTap;
                }
            //}
        } 
        if (timer < 0 /*&& musicPlayer.GetComponent<MusicScript>().readyToPlay*/)
        {
            //print("end " + Time.time);
            timingFailure += 1;
            currentTiming = false;
            timingState = WaitingForMissedTap;
        }
    }

    void WaitingForMissedTap()
    {
        //if (musicPlayer.GetComponent<MusicScript>().readyToPlay)
        //{
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                //print("tap " + Time.time);
                timingFailure += 1;
                currentTiming = false;
                //print("bad");
            }
        //}
    }

    void WaitForCountDownEnd()
    {
        timer = 0;
        countDownTime = 2;
        ready = false;
    }
}