﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpScript : MonoBehaviour
{

    public GameObject musicPlayer;
    public int currentNumber = 0;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Audio"))
        {
            currentNumber++;
            if (currentNumber == other.gameObject.GetComponent<AudioPickUp>().audioContainers.Count)
            {
                currentNumber = 0;
            }
            AudioClip nextClip = other.gameObject.GetComponent<AudioPickUp>().audioContainers[currentNumber].clip;
            AudioClip currentClip = musicPlayer.GetComponent<MusicScript>().clip;

            /*if (randomClip.name.Equals(currentClip.name))
            {
                if (randomNumber + 1 == other.gameObject.GetComponent<AudioPickUp>().audioContainers.Count)
                {
                    randomNumber = 0;
                }
                randomClip = other.gameObject.GetComponent<AudioPickUp>().audioContainers[randomNumber].clip;
            }*/
            Destroy(other.gameObject);
            transform.position = GetComponent<CharacterMoving>().DashDestination;
            musicPlayer.GetComponent<MusicScript>().clip = nextClip;
            musicPlayer.GetComponent<MusicScript>().SongStart();
        }
    }
}
