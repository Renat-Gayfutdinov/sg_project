﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlowScript : PausableObject
{
    Image image;
    public GameObject musicPlayer;
    public GameObject Character;
    // Use this for initialization
    void Start()
    {
        image = GetComponent<Image>();
        musicPlayer.GetComponent<MusicScript>().beatEvent.AddListener(TurnOnLight);
    }

    // Update is called once per frame
    void Update()
    {
        if (musicPlayer.GetComponent<AudioSource>().isPlaying)
        {
            Color color = image.color;

            if (Character.GetComponent<TimingManager>().currentTiming)
            {
                color.r = 0;
                color.g = 255;
                color.b = 0;
            }
            else
            {
                color.r = 255;
                color.g = 0;
                color.b = 0;
            }

            if (color.a > 0)
            {
                color.a -= 0.025f;
            }

            image.color = color;
        }
    }

    void TurnOnLight ()
    {
        Color color = image.color;
        color.a = 1f;
        image.color = color;
    }
}