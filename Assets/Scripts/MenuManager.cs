﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public GameObject pointsManager;
    delegate void PauseState();
    PauseState pauseState;
    public GameObject menuPanel;
    public GameObject allObjects;
    public UnityEvent pauseEvent;
    public UnityEvent playEvent;

    Button playButton;
    Button mainMenuButton;

	// Use this for initialization
	void Start ()
    {
        playButton = menuPanel.transform.GetChild(0).gameObject.GetComponent<Button>();
        mainMenuButton = menuPanel.transform.GetChild(1).gameObject.GetComponent<Button>();

        playButton.onClick.AddListener(Continue);
        mainMenuButton.onClick.AddListener(GoToMainMenu);

        if (pauseEvent == null)
        {
            pauseEvent = new UnityEvent();
        }

        if (playEvent == null)
        {
            playEvent = new UnityEvent();
        }

        pointsManager.GetComponent<PointsManager>().deathEvent.AddListener(ChangePauseState);
        //print(pointsManager.name);
        pauseState = Play;
	}

    void Continue ()
    {
        if (pauseState == Pause)
        {
            menuPanel.SetActive(false); 
            playEvent.Invoke();
            pauseState = Play;
        }
    }

    void GoToMainMenu ()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    void ChangePauseState ()
    {
        if (pauseState == Play)
        {
            pauseState = Pause;
        } else
        {
            pauseState = Play;
        }
    }

    void Play ()
    {
        
    }

    void Pause ()
    {
        
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (pauseState == Play)
            {
                pauseEvent.Invoke();
                pauseState = Pause;
                menuPanel.SetActive(true);
            }
        }
        pauseState();
	}
}
