﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiation : MonoBehaviour
{
    private Initialization initialization;
    private Obstacle_gen obst_gen;

    private CharacterMoving chMove;
    private Vector3 wpos;
    private bool wp = false;
    private int pp = 0;
    private void Start()
    {
        initialization = GameObject.FindGameObjectWithTag("Loader").GetComponent<Initialization>();
        obst_gen = GameObject.FindGameObjectWithTag("Loader").GetComponent<Obstacle_gen>();
        chMove = GetComponent<CharacterMoving>();
        initialization.GenerationObjects = new List<Transform>();
        initialization.Gs = new List<int>();
        CreateFirstPref();
        Init();
    }

    private void Update()
    {
        // if (chMove.ReadyToTurn && wp)
        // {
        //     chMove.DashDestination = new Vector3(wpos.x, transform.position.y, wpos.z);
        //     transform.position = new Vector3(wpos.x, transform.position.y, wpos.z);
        //     wp = false;
        // }
    }

    private void OnTriggerEnter(Collider other)
    {
        Boolean check = false;
        int index = 0;
        if (other.GetComponent<Transform>().tag == "TriggerDelete")
        {
            for (int i = 0; i < initialization.GenerationObjects.Count; i++)
            {
                if (other.GetComponent<Transform>().parent.GetComponent<Transform>().name.Equals(initialization.GenerationObjects[i].name))
                {
                    if (i == initialization.GenerationObjects.Count - 5)
                    {
                        DoInit(initialization.GenerationObjects[initialization.GenerationObjects.Count - 1].GetChild(0).GetComponent<Transform>().GetComponent<Collider>());
                        RandomPlacedPrefabs();
                    }
                    if (i == 2)
                    {
                        if (!check)
                        {
                            Destroy(initialization.GenerationObjects[0].gameObject);
                            initialization.GenerationObjects.RemoveAt(0);
                            check = true;
                        }
                    }
                }
            }
        }

        if (other.tag == "localEmpty_l")
        {
            // wp = true;
            // GameObject ot = other.gameObject;
            // wpos = ot.transform.GetChild(0).TransformPoint(Vector3.zero);
            // initialization.DirectCharacter -= 90;

            // GetComponent<Transform>().rotation = Quaternion.Euler(0, initialization.DirectCharacter, 0);
            // transform.position = new Vector3(wpos.x, transform.position.y, wpos.z);

            for (int i = 4; i < 7; i++)
                DisableTriggers(i, other);
        }
        if (other.tag == "localEmpty_r")
        {
            // wp = true;
            // GameObject ot = other.gameObject;
            // wpos = ot.transform.GetChild(0).TransformPoint(Vector3.zero);
            // initialization.DirectCharacter += 90;

            // GetComponent<Transform>().rotation = Quaternion.Euler(0, initialization.DirectCharacter, 0);
            // transform.position = new Vector3(wpos.x, transform.position.y, wpos.z);

            for (int i = 4; i < 7; i++)
                DisableTriggers(i, other);
        }
        if (other.tag == "Respawn")
        {
            DoInit(other);
            RandomPlacedPrefabs();
            foreach (Transform t in initialization.GenerationObjects)
            {
                if (other.GetComponent<Transform>().parent.GetComponent<Transform>().name.Equals(t.name))
                {
                    for (int i = 0; i < initialization.GenerationObjects.Count; i++)
                    {
                        if (t.Equals(initialization.GenerationObjects[i]))
                        {
                            index = i;
                        }
                    }
                    if (index == initialization.CountGenerations - 4)
                    {
                        DoInit(initialization.GenerationObjects[initialization.GenerationObjects.Count - 1].GetChild(0).GetComponent<Transform>().GetComponent<Collider>());
                        RandomPlacedPrefabs();
                    }
                    if (index == 2)
                    {
                        Destroy(initialization.GenerationObjects[0].gameObject);
                        initialization.GenerationObjects.RemoveAt(0);
                        break;
                    }
                }
            }
        }
    }

    private void DisableTriggers(int id, Collider other)
    {
        other.GetComponent<Transform>().parent.GetChild(id).GetComponent<Collider>().enabled = false;
    }

    private void RandomPlacedPrefabs()
    {

        for (int j = 0; j < initialization.CountGenerations; j++)
        {
            RandomFunc ranD = RandomFunc();
            string tag_C = ranD.getTag();
            if (tag_C == "Corridor")
            {
                initialization.D_right = false;
            }
            else if (tag_C == "Corridor_Left")
            {
                initialization.D_left = true;
                initialization.D_right = false;
            }
            else if (tag_C == "Corridor_Right")
            {
                initialization.D_right = true;
                initialization.D_left = false;
            }

            initialization.GenerationStates.Add(tag_C);
            initialization.Gs.Add(ranD.getId());

            GameObject go = initialization.GO1;
            DoInit(go);
            if (initialization.GenerationStates.Count != 0)
            {
                if (initialization.GenerationStates[0] == "Corridor")
                {
                    DoCorridor(initialization.Gs[0]);
                }
                else if (initialization.GenerationStates[0] == "Corridor_Left")
                {
                    DoDegreeLeft(initialization.Gs[0]);

                }
                else if (initialization.GenerationStates[0] == "Corridor_Right")
                {
                    DoDegreeRight(initialization.Gs[0]);
                }
                initialization.GenerationStates.RemoveAt(0);
                initialization.Gs.RemoveAt(0);
            }
            else
            {
                break;
            }
        }
    }

    private RandomFunc RandomFunc()
    {
        RandomFunc ranD = new RandomFunc();
        int randomDirect = UnityEngine.Random.Range(0, initialization.Prefabs.Count);
        if (initialization.Prefabs[randomDirect].tag == "Corridor_Left" && initialization.D_left == true)
        {
            return RandomFunc();
        }

        if (initialization.Prefabs[randomDirect].tag == "Corridor_Right" && initialization.D_right == true)
        {
            return RandomFunc();
        }

        if (FindPrefabsWithTag(initialization.Prefabs[randomDirect].tag))
        {
            ranD.setTag(initialization.Prefabs[randomDirect].tag);
            ranD.setId(randomDirect);
        }
        else
        {
            Debug.Log("");
            if (pp != 100)
            {
                pp++;
                return RandomFunc();
            }
        }
        return ranD;
    }

    private Boolean FindPrefabsWithTag(String tag_C)
    {
        Boolean check = true;
        foreach (GameObject pr in initialization.Prefabs)
        {
            if (pr.tag.Equals(tag_C))
            {
                check = CheckCollisions(pr);
                break;
            }
        }
        return check;
    }

    private Boolean CheckCollisions(GameObject pr)
    {
        Boolean check = true;
        Vector3 pr_scale = pr.GetComponent<MeshFilter>().sharedMesh.bounds.size;
        GameObject go_F = initialization.GO1;
        DoInitFictive(go_F);
        Vector3 pr_position = initialization.WorldPosPivor_F1;
        for (int i = 0; i < initialization.GenerationObjects.Count - 1; i++)
        {
            if (24f >= Vector3.Distance(initialization.GenerationObjects[i].position, pr_position))
            {
                check = false;
            }
        }
        return check;
    }

    private void DoCorridor(int d)
    {
        Vector3 localCorRot = initialization.LocalRotate.eulerAngles;
        initialization.GO1 = Instantiate(initialization.Prefabs[d], initialization.WorldPosPivor, Quaternion.Euler(localCorRot.x,
            localCorRot.y + initialization.DirectCorridor, localCorRot.z));
        initialization.D_right = false;
        initialization.GO1.GetComponent<Transform>().name = "Object" + initialization.NameId++;
        initialization.GenerationObjects.Add(initialization.GO1.GetComponent<Transform>());
        initialization.CountGenRew++;
        obst_gen.SetObjectForObstacles(initialization.GO1);
    }

    private void DoDegreeLeft(int dl)
    {
        initialization.Loc_D = new Vector3(initialization.Loc_D.x, initialization.Loc_D.y + initialization.DirectLeftDegree, initialization.Loc_D.z);
        initialization.GO1 = Instantiate(initialization.Prefabs[dl], initialization.WorldPosPivor, Quaternion.Euler(initialization.Loc_D));
        initialization.DirectRightDegree -= 90;
        initialization.DirectCorridor -= 90;
        initialization.DirectLeftDegree -= 90;
        initialization.D_left = true;
        initialization.D_right = false;
        initialization.GO1.GetComponent<Transform>().name = "Object" + initialization.NameId++;
        initialization.GenerationObjects.Add(initialization.GO1.GetComponent<Transform>());
        //initialization.CountGenRew++;
        //obst_gen.SetObjectForObstacles(initialization.GO1);
    }

    private void DoDegreeRight(int dr)
    {

        initialization.Loc_D = new Vector3(initialization.Loc_D.x, initialization.Loc_D.y + initialization.DirectRightDegree, initialization.Loc_D.z);
        initialization.GO1 = Instantiate(initialization.Prefabs[dr], initialization.WorldPosPivor, Quaternion.Euler(initialization.Loc_D));
        initialization.DirectLeftDegree += 90;
        initialization.DirectCorridor += 90;
        initialization.DirectRightDegree += 90;
        initialization.D_right = true;
        initialization.D_left = false;
        initialization.GO1.GetComponent<Transform>().name = "Object" + initialization.NameId++;
        initialization.GenerationObjects.Add(initialization.GO1.GetComponent<Transform>());
        //initialization.CountGenRew++;
        //obst_gen.SetObjectForObstacles(initialization.GO1);
    }

    private void DoInit(Collider other)
    {
        initialization.LocalPivot = other.GetComponent<Transform>().parent.GetChild(1).GetComponent<Transform>();
        initialization.WorldPosPivor = initialization.LocalPivot.TransformPoint(Vector3.zero);
        initialization.LocalRotate = initialization.LocalPivot.localRotation;
        initialization.Loc_D = initialization.LocalPivot.eulerAngles;
    }

    private void DoInit(GameObject go)
    {
        initialization.LocalPivot = go.GetComponent<Transform>().GetChild(1).GetComponent<Transform>();
        initialization.WorldPosPivor = initialization.LocalPivot.TransformPoint(Vector3.zero);
        initialization.LocalRotate = initialization.LocalPivot.localRotation;
        initialization.Loc_D = initialization.LocalRotate.eulerAngles;
        go.GetComponent<Transform>().GetChild(0).tag = "TriggerDelete";
    }


    private void DoInitFictive(GameObject go_F)
    {
        initialization.LocalPivot_F = go_F.GetComponent<Transform>().GetChild(1).GetComponent<Transform>();
        initialization.WorldPosPivor_F1 = initialization.LocalPivot_F.TransformPoint(Vector3.zero);
        initialization.LocalRotate_F = initialization.LocalPivot_F.localRotation;
        initialization.Loc_D_F = initialization.LocalRotate_F.eulerAngles;
        go_F.GetComponent<Transform>().GetChild(0).tag = "TriggerDelete";
    }
    private void DeleteBefore(GameObject del)
    {
        Destroy(del, 5);
    }

    private void Init()
    {
        initialization.DirectCorridor = 0;
        initialization.DirectLeftDegree = 0;
        initialization.DirectRightDegree = 0;
        initialization.DirectCharacter = 90;
        initialization.GenerationStates = new List<String>();
    }

    private void DestroyObjectDelayed(GameObject gameObject)
    {
        Destroy(gameObject, initialization.TimerDelete);
    }

    private void CreateFirstPref()
    {
        initialization.GO1 = Instantiate(initialization.Prefabs[0], Vector3.zero, new Quaternion());
        initialization.GO1.GetComponent<Transform>().name = "Object" + initialization.NameId++;
        initialization.GenerationObjects.Add(initialization.GO1.GetComponent<Transform>());
        initialization.CountGenRew = 1;
    }

    #region
    private void CalculateDistance(Transform goT)
    {
        float dist = Vector3.Distance(transform.TransformPoint(goT.position), GetComponent<Transform>().localPosition);
        Debug.Log(dist + " " + initialization.GenerationStates.Count);
        if (Mathf.Abs(dist - initialization.Distance) > initialization.Epsilon)
        {
            foreach (Transform obj in initialization.GenerationObjects)
            {
                if (obj.name == goT.name)
                {
                    DestroyObjects(goT);
                }
            }
        }
    }
    private void DestroyObjects(Transform goT)
    {
        Destroy(goT.gameObject);
    }
    #endregion
}
