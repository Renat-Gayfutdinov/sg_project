using System.Collections.Generic;
using UnityEngine;

public class Obstacle_gen : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> obstacles;
    [SerializeField]
    private List<GameObject> rewards;
    [SerializeField]
    private int randomCount = 3;
    [SerializeField]
    private int sequenceReward = 0;
    private List<GameObject> pivotsObst;
    private bool[] visits;
    private Initialization initialization;
    private void Start()
    {
        initialization = GameObject.FindGameObjectWithTag("Loader").GetComponent<Initialization>();
    }
    public void SetObjectForObstacles(GameObject obj)
    {
        obj.AddComponent<GameObjectSearcher>();
        GameObjectSearcher gameObjectSearcher = obj.GetComponent<GameObjectSearcher>();
        gameObjectSearcher.SearchTag = "Pivot_obstacle";
        gameObjectSearcher.StartFind();
        pivotsObst = gameObjectSearcher.ObstPivots;
        visits = new bool[pivotsObst.Count];
        RandomFunc(obj);
    }

    private void RandomFunc(GameObject obj)
    {
        float r = 0;
        for (int i = 0; i < randomCount; i++)
        {
            int a = UnityEngine.Random.Range(0, pivotsObst.Count);
            int b = UnityEngine.Random.Range(0, obstacles.Count);
            if(obstacles[b].tag != "Barrier_static"){
                r = UnityEngine.Random.Range(0f, 360f);
            }else{
               r = 0;
            }
            if (!visits[a] && CheckQueue(a))
            {
                GameObject go = Instantiate(obstacles[b], pivotsObst[a].GetComponent<Transform>().position, Quaternion.Euler(0, r, 0));
                go.GetComponent<Transform>().SetParent(obj.GetComponent<Transform>());
                visits[a] = true;
            }
            else
            {
                i--;
            }
        }
        if(initialization.CountGenRew == sequenceReward){
            initialization.CountGenRew = 0;
            Debug.Log("GJGJG");
            RandomFuncRewards(obj);
        }
    }

    private void RandomFuncRewards(GameObject obj)
    {
        for (int i = 0; i < 1; i++)
        {
            int a = UnityEngine.Random.Range(0, pivotsObst.Count);
            int b = UnityEngine.Random.Range(0, rewards.Count);
            if (!visits[a])
            {
                GameObject go = Instantiate(rewards[b], pivotsObst[a].GetComponent<Transform>().position, new Quaternion());
                go.GetComponent<Transform>().SetParent(obj.GetComponent<Transform>());
                visits[a] = true;
            }
            else
            {
                i--;
            }
        }
    }

    private bool CheckQueue(int a)
    {
        //check Horizontal
        for (int i = 0; i < visits.Length - 2; i += 3)
        {
            for (int j = 0; j < 3; j++)
            {
                if ((visits[i] && visits[i + 1] && a == i + 2) || (visits[i] && a == i + 1 && visits[i + 2]) || (a == i && visits[i + 1] && visits[i + 2]))
                {
                    return false;
                }
            }
        }
        return true;
    }

}