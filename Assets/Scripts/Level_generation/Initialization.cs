﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialization : MonoBehaviour {

    #region
    [SerializeField]
    private List<GameObject> prefabs;
    [SerializeField]
    private GameObject character;
    [SerializeField]
    private int countGenerations = 1;
    [SerializeField]
    private float timerDelete = 5.0f;
    [SerializeField]
    private float distance = 10.0f;
    [SerializeField]
    private float distanceRay = 1.0f;
    [SerializeField]
    private float speedH = 2.0f;
    [SerializeField]
    private float speedV = 2.0f;
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private float bulletForce = 10f;
    [SerializeField]
    private float timeDeleteBullet = 2f;
    private int countGenRew;
    #endregion

    private int nameId = 0;
    private int epsilon = 10;

    private List<String> generationStates;
    private List<Transform> generationObjects;
    private GameObject GO;
    private Vector3 worldPosPivor, WorldPosPivor_F;
    private Vector3 loc_D, loc_D_F;
    private Quaternion localRotate, localRotate_F;
    private Transform localPivot, localPivot_F;
    private Boolean d_left;
    private Boolean d_right;
    private int directCorridor;
    private int directLeftDegree;
    private int directRightDegree;
    private int directCharacter;
    private Initialization initialization;
    private List<int> gs;

    public float TimerDelete
    {
        get
        {
            return timerDelete;
        }

        set
        {
            timerDelete = value;
        }
    }

    public int CountGenerations
    {
        get
        {
            return countGenerations;
        }

        set
        {
            countGenerations = value;
        }
    }

    public float Distance
    {
        get
        {
            return distance;
        }

        set
        {
            distance = value;
        }
    }

    public int NameId
    {
        get
        {
            return nameId;
        }

        set
        {
            nameId = value;
        }
    }

    public int Epsilon
    {
        get
        {
            return epsilon;
        }

        set
        {
            epsilon = value;
        }
    }

    public GameObject Character
    {
        get
        {
            return character;
        }

        set
        {
            character = value;
        }
    }

    public List<GameObject> Prefabs
    {
        get
        {
            return prefabs;
        }

        set
        {
            prefabs = value;
        }
    }

    public float DistanceRay
    {
        get
        {
            return distanceRay;
        }

        set
        {
            distanceRay = value;
        }
    }

    public float SpeedH
    {
        get
        {
            return speedH;
        }

        set
        {
            speedH = value;
        }
    }

    public float SpeedV
    {
        get
        {
            return speedV;
        }

        set
        {
            speedV = value;
        }
    }

    public List<String> GenerationStates
    {
        get
        {
            return generationStates;
        }

        set
        {
            generationStates = value;
        }
    }

    public List<Transform> GenerationObjects
    {
        get
        {
            return generationObjects;
        }

        set
        {
            generationObjects = value;
        }
    }

    public GameObject GO1
    {
        get
        {
            return GO;
        }

        set
        {
            GO = value;
        }
    }

    public Vector3 WorldPosPivor
    {
        get
        {
            return worldPosPivor;
        }

        set
        {
            worldPosPivor = value;
        }
    }

    public Vector3 Loc_D
    {
        get
        {
            return loc_D;
        }

        set
        {
            loc_D = value;
        }
    }

    public Quaternion LocalRotate
    {
        get
        {
            return localRotate;
        }

        set
        {
            localRotate = value;
        }
    }

    public Boolean D_left
    {
        get
        {
            return d_left;
        }

        set
        {
            d_left = value;
        }
    }

    public Boolean D_right
    {
        get
        {
            return d_right;
        }

        set
        {
            d_right = value;
        }
    }

    public int DirectCorridor
    {
        get
        {
            return directCorridor;
        }

        set
        {
            directCorridor = value;
        }
    }

    public int DirectLeftDegree
    {
        get
        {
            return directLeftDegree;
        }

        set
        {
            directLeftDegree = value;
        }
    }

    public int DirectRightDegree
    {
        get
        {
            return directRightDegree;
        }

        set
        {
            directRightDegree = value;
        }
    }

    public int DirectCharacter
    {
        get
        {
            return directCharacter;
        }

        set
        {
            directCharacter = value;
        }
    }
    public List<int> Gs
    {
        get
        {
            return gs;
        }

        set
        {
            gs = value;
        }
    }

    public Transform LocalPivot_F
    {
        get
        {
            return localPivot_F;
        }

        set
        {
            localPivot_F = value;
        }
    }

    public Vector3 WorldPosPivor_F1
    {
        get
        {
            return WorldPosPivor_F;
        }

        set
        {
            WorldPosPivor_F = value;
        }
    }

    public Quaternion LocalRotate_F
    {
        get
        {
            return localRotate_F;
        }

        set
        {
            localRotate_F = value;
        }
    }

    public Vector3 Loc_D_F
    {
        get
        {
            return loc_D_F;
        }

        set
        {
            loc_D_F = value;
        }
    }

    public Transform LocalPivot
    {
        get
        {
            return localPivot;
        }

        set
        {
            localPivot = value;
        }
    }

    public GameObject Bullet
    {
        get
        {
            return bullet;
        }

        set
        {
            bullet = value;
        }
    }

    public int CountGenRew
    {
        get
        {
            return countGenRew;
        }

        set
        {
            countGenRew = value;
        }
    }

    public float BulletForce
    {
        get
        {
            return bulletForce;
        }

        set
        {
            bulletForce = value;
        }
    }

    public float TimeDeleteBullet
    {
        get
        {
            return timeDeleteBullet;
        }

        set
        {
            timeDeleteBullet = value;
        }
    }
}
