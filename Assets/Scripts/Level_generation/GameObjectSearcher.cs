using UnityEngine;
 using System.Collections;
 using System.Collections.Generic;
 
 public class GameObjectSearcher : MonoBehaviour
 {
    private string searchTag;
    private List<GameObject> obstPivots = new List<GameObject>();

    public string SearchTag
    {
        get
        {
            return searchTag;
        }

        set
        {
            searchTag = value;
        }
    }

    public List<GameObject> ObstPivots
    {
        get
        {
            return obstPivots;
        }

        set
        {
            obstPivots = value;
        }
    }
    
    public void StartFind(){
        if (SearchTag != null)
         {
             FindObjectwithTag(SearchTag);
         }
    }
     public void FindObjectwithTag(string _tag)
     {
         ObstPivots.Clear();
         Transform parent = transform;
         GetChildObject(parent, _tag);
     }
 
     public void GetChildObject(Transform parent, string _tag)
     {
         for (int i = 0; i < parent.childCount; i++)
         {
             Transform child = parent.GetChild(i);
             if (child.tag == _tag)
             {
                 ObstPivots.Add(child.gameObject);
             }
             if (child.childCount > 0)
             {
                 GetChildObject(child, _tag);
             }
         }
     }
 }