public class RandomFunc
{
    private string tagC;
    private int idC;

    public RandomFunc(){
        
    }

    public RandomFunc(string tagC, int idC)
    {
        this.tagC = tagC;
        this.idC = idC;
    }

    public string getTag(){
        return this.tagC;
    }

    public void setTag(string tagC){
        this.tagC = tagC;
    }

    public int getId(){
        return this.idC;
    }

    public void setId(int idC){
        this.idC = idC;
    }

}