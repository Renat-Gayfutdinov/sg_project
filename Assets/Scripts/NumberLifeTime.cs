﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberLifeTime : MonoBehaviour {

    public float lifeTime;

	// Use this for initialization
	void Start () {
        lifeTime = 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
		if (lifeTime > 0 && this.gameObject.activeInHierarchy)
        {
            lifeTime -= Time.deltaTime;
            transform.localScale -= new Vector3(1.0f, 1.0f, 1.0f) * Time.deltaTime * 0.8f;
        } else
        {
            this.gameObject.SetActive(false);
            lifeTime = 0.5f;
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
	}
}
