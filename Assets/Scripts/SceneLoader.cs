﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoader : MonoBehaviour
{

    //public string pathToLevel;
    public GameObject character;
    public GameObject musicPlayer;
    public GameObject canvas;
    public GameObject lightningSystem;

    // Use this for initialization

    void Start()
    {
        
    }
    void Awake()
    {
        Vector3 position = Vector3.up * 0.5f;
        
        character = Instantiate(character, position, Quaternion.identity);
        musicPlayer = Instantiate(musicPlayer);
        canvas = Instantiate(canvas);
        //lightningSystem = Instantiate(lightningSystem);
         
        canvas.GetComponent<Transform>().GetChild(0).GetComponent<GlowScript>().musicPlayer = musicPlayer;
        canvas.GetComponent<Transform>().GetChild(0).GetComponent<GlowScript>().Character = character;

        GetComponent<MenuManager>().pointsManager = character;
        GetComponent<MenuManager>().menuPanel = canvas.GetComponent<Transform>().GetChild(3).gameObject;

        GetComponent<CountDownTimer>().musicPlayer = musicPlayer;
        GetComponent<CountDownTimer>().numbers = canvas.GetComponent<Transform>().GetChild(2).gameObject;

        lightningSystem.GetComponent<LightningSystemScript>().musicPlayer = musicPlayer;
        lightningSystem.GetComponent<LightningSystemScript>().character = character;

        musicPlayer.GetComponent<MusicScript>().timingManager = character;
        musicPlayer.GetComponent<MusicScript>().menuManager = gameObject;

        character.GetComponent<TimingManager>().menuManager = gameObject;
        character.GetComponent<CharacterMoving>().menuManager = gameObject;

        character.transform.position = position;
        character.GetComponent<CharacterMoving>().musicPlayer = musicPlayer;
        character.GetComponent<TimingManager>().musicPlayer = musicPlayer;
        character.GetComponent<PointsManager>().health = canvas.transform.GetChild(1).gameObject;
        character.GetComponent<PickUpScript>().musicPlayer = musicPlayer;
    }

    // Update is called once per frame
    void Update()
    {

    }
}