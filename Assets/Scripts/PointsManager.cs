﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PointsManager : MonoBehaviour {
   
    public GameObject health;
    public int healthCount;
    delegate void PlayerState();
    PlayerState playerState;
    public UnityEvent deathEvent;

	// Use this for initialization
	void Start ()
    {
        if (deathEvent == null)
        {
            deathEvent = new UnityEvent();
        }

        GetComponent<TimingManager>().FailureEvent.AddListener(TakeDamage);
        playerState = Alive;
	}
	
	// Update is called once per frame
	void Update () {
        playerState();
	}

    void Alive ()
    {

    }

    void Dead ()
    {
            
    }

    void TakeDamage ()
    {
       

        if (healthCount > 0)
        {
            Destroy(health.GetComponent<Transform>().GetChild(healthCount).gameObject);
            healthCount--;
        } else
        {
            deathEvent.Invoke();
            playerState = Dead;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Barrier"))
        {
            TakeDamage();
        }
    }
}
