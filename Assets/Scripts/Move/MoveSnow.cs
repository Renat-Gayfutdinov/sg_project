﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSnow : MonoBehaviour
{

    [SerializeField]
    private GameObject snow;
    [SerializeField]
    private GameObject character;

    private GameObject go;

    public GameObject Snow
    {
        get
        {
            return snow;
        }

        set
        {
            snow = value;
        }
    }

    public GameObject Character
    {
        get
        {
            return character;
        }

        set
        {
            character = value;
        }
    }

    private void Start()
    {
        go = Instantiate(snow);
    }

    private void Update()
    {
        go.GetComponent<Transform>().SetPositionAndRotation(new Vector3(Character.GetComponent<Transform>().position.x, go.GetComponent<Transform>().position.y,
            Character.GetComponent<Transform>().position.z), new Quaternion());
    }
}
