﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMouse : MonoBehaviour
{
    private float yaw = 0.0f;
    private float pitch = 0.0f;
    private Initialization initialization;

    private void Start()
    {
        initialization = GameObject.FindGameObjectWithTag("Loader").GetComponent<Initialization>();
        Cursor.visible = false;
    }

    private Vector3 BallisticVel(Transform target, float angle)
    {

        var dir = target.position - transform.position;  // get target direction
        var h = dir.y;  // get height difference
        dir.y = 0;  // retain only the horizontal direction
        var dist = dir.magnitude;  // get horizontal distance
        var a = angle * Mathf.Deg2Rad;  // convert angle to radians
        dir.y = dist * Mathf.Tan(a);  // set dir to the elevation angle
        dist += h / Mathf.Tan(a);  // correct for small height differences
        // calculate the velocity magnitude
        var vel = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return vel * dir.normalized;

    }

    void FixedUpdate()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward) * initialization.DistanceRay;
        //Debug.DrawRay(transform.position, forward, Color.green);

        RaycastHit hit;
        Ray ray = new Ray(transform.position, forward);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, initialization.DistanceRay))
            {
                Transform objectHit = hit.transform;
                GameObject ball = Instantiate(initialization.Bullet, transform.position, Quaternion.identity);
                ball.GetComponent<Rigidbody>().AddForceAtPosition(transform.forward * initialization.BulletForce, objectHit.position, ForceMode.Impulse);
                // ball.GetComponent<Rigidbody>().velocity += BallisticVel(objectHit, 30);
                Destroy(ball, initialization.TimeDeleteBullet);
            }else{
                GameObject ball = Instantiate(initialization.Bullet, transform.position, Quaternion.identity);
                ball.GetComponent<Rigidbody>().AddForce(transform.forward * initialization.BulletForce, ForceMode.Impulse);
                Destroy(ball, initialization.TimeDeleteBullet);
            }
        }

        Quaternion rotateParent = GetComponent<Transform>().parent.GetComponent<Transform>().localRotation;

        yaw += initialization.SpeedH * Input.GetAxis("Mouse X");
        pitch -= initialization.SpeedV * Input.GetAxis("Mouse Y");

        if (yaw > 45)
        {
            yaw = 45;
        }
        else if (yaw < -45)
        {
            yaw = -45;
        }
        if (pitch > 45)
        {
            pitch = 45;
        }
        else if (pitch < -45)
        {
            pitch = -45;
        }
        transform.localRotation = Quaternion.Euler(pitch, yaw, 0f);
    }
}

