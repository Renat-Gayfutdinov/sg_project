﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MusicScript : MonoBehaviour {

    public float bpm;
    public GameObject menuManager;

    //the current position of the song (in seconds)
    public float songPosition;

    //the current position of the song (in beats)
    public float songPosInBeats;

    //the duration of a beat
    public float secPerBeat;

    //how much time (in seconds) has passed since the song started
    public float dsptimesong;

    public float nextBeatTime;

    public float beatDelta;
    public float beatDeltaAbs;
    public float beatTime;

    public float preTimingTime;
    public float preTimingDelta;

    public float songStartTime;

    public float latancy;
    public float timingDuration;

    public UnityEvent beatEvent;
    public UnityEvent songStartEvent;
    public UnityEvent timingEvent;
    public UnityEvent endTimingEvent;
    public UnityEvent countDownEvent;

    public GameObject timingManager;

    public AudioClip clip;

    public bool readyToPlay;

    // Use this for initialization
    void Start () {

        menuManager.GetComponent<MenuManager>().pauseEvent.AddListener(Pause);
        menuManager.GetComponent<MenuManager>().playEvent.AddListener(Play);

        readyToPlay = false;

        if (beatEvent == null)
        {
            beatEvent = new UnityEvent();
        }

        if (timingEvent == null)
        {
            timingEvent = new UnityEvent();
        }

        if (endTimingEvent == null)
        {
            endTimingEvent = new UnityEvent();
        }

        if (countDownEvent == null)
        {
            countDownEvent = new UnityEvent();
        }

        clip = GetComponent<AudioSource>().clip;
        //calculate how many seconds is one beat
        //we will see the declaration of bpm later
        //Application.targetFrameRate = 30;  
        //SongStart();
        //readyToPlay = true;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.Return) && !GetComponent<AudioSource>().isPlaying)
        {
            SongStart();
            readyToPlay = true;
        }

        if (readyToPlay)
        {
            if (GetComponent<AudioSource>().isPlaying)
            {
                DetectBeatTime();
            }
        }
    }

    void Play ()
    {
        //print("play");
        //SongStart(false);
        //countDownEvent.Invoke();
        //GetComponent<AudioSource>().Play();
        SongStart();
        //DetectBeatTime();
        readyToPlay = true;
    }

    void Pause ()
    {
        GetComponent<AudioSource>().Stop();
        readyToPlay = false;
    }

    public void SongStart()
    {
        //record the time when the song starts
        nextBeatTime = 0;
        preTimingTime = - timingDuration;
        beatDelta = 0;
        preTimingDelta = 0;
        songPosInBeats = 0;
        //start the song
        GetComponent<AudioSource>().clip = clip;
        GetComponent<AudioSource>().time = 0;
        dsptimesong = (float)AudioSettings.dspTime;
        songPosition = (float)(AudioSettings.dspTime - dsptimesong);
        GetComponent<AudioSource>().Play();
        secPerBeat = 60f / bpm;
        countDownEvent.Invoke();
    }

    void DetectBeatTime()
    {
        //calculate the position in seconds
        songPosition = (float)(AudioSettings.dspTime - dsptimesong);

        //calculate the position in beats
        songPosInBeats = songPosition / secPerBeat;

        preTimingDelta = songPosition - preTimingTime;
        beatDelta = songPosition - nextBeatTime;

        beatDeltaAbs = Mathf.Abs(nextBeatTime - songPosition);

        /*if (songPosInBeats > 3)
        {
            if (beatDelta > - latancy)
            {
                songStartTime = Time.time;
                readyToPlay = true;
                //timingManager.GetComponent<TimingManager>().beatCenter = songStartTime;
                //print("startTime" + Time.time);
            }
        }*/

        if (beatDelta > - latancy)
        {
            /*if (songPosInBeats > 4)
            {
                beatEvent.Invoke();
            }*/
            beatTime = Time.time;
            //print("beat" + Time.time);
             
            nextBeatTime += secPerBeat;
            beatEvent.Invoke();
        }

        /*if (preTimingDelta > - latancy)
        {
            preTimingTime += secPerBeat;
            timingEvent.Invoke();
        }*/
        if (preTimingTime == - timingDuration)
        {
            preTimingTime += secPerBeat;
        } else
        {
            if (preTimingDelta > - latancy)
            {
                //print("pretiming");
                preTimingTime += secPerBeat;
                timingEvent.Invoke();
            }
        }
    }
}