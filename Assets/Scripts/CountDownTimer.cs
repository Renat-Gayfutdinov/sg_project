﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDownTimer : MonoBehaviour {

    public int countDownTime;
    public GameObject musicPlayer;

    delegate void CountDownState();
    CountDownState countDownState;

    public GameObject numbers;

    // Use this for initialization
    void Start () {
        musicPlayer.GetComponent<MusicScript>().beatEvent.AddListener(CountDown);
        musicPlayer.GetComponent<MusicScript>().countDownEvent.AddListener(LaunchCountDown);
        countDownState = Idle;
    }
	
	// Update is called once per frame
	void Update () {
        countDownState();	
	}

    void CountingDown()
    {

    }

    void Idle()
    {

    }

    void LaunchCountDown ()
    {
        //print("Launch");
        countDownTime = 4;
        countDownState = WaitForCountDownEnd;
    }

    void WaitForCountDownEnd()
    {
        if (countDownTime == 0)
        {
            countDownState = Idle;
        }
    }

    void CountDown()
    {
        if (countDownTime > 0)
        {
            numbers.transform.GetChild(countDownTime - 1).gameObject.SetActive(true);
            //print("countDown" + countDownTime);
            countDownTime--;
        } 
    }
}