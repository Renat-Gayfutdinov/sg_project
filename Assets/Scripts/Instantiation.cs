﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Instantiation : MonoBehaviour
{
    [SerializeField]
    public GameObject prefab_corr;
    [SerializeField]
    public GameObject prefab_right;
    [SerializeField]
    public GameObject prefab_left;
    [SerializeField]
    private GameObject character;
    [SerializeField]
    private int countGenerations = 1;
    [SerializeField]
    private float timerDelete = 5;

    private List<int> generationStates;
    private GameObject GO;
    private Vector3 worldPosPivor;
    private Vector3 loc_D;
    private Quaternion localRotate;
    private Transform localPivot;
    private Boolean d_left;
    private Boolean d_right;
    private int directCorridor;
    private int directLeftDegree;
    private int directRightDegree;
    private int directCharacter;

    public float TimerDelete
    {
        get
        {
            return timerDelete;
        }

        set
        {
            timerDelete = value;
        }
    }

    public int CountGenerations
    {
        get
        {
            return countGenerations;
        }

        set
        {
            countGenerations = value;
        }
    }

    private void Start()
    {
        CreateFirstPref();
        MoveCharacter();
        Init();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Transform>().tag == "TriggerDelete")
        {
            DestroyObjectDelayed(other.GetComponent<Transform>().parent.gameObject);
        }

        if (other.tag == "localEmpty_l")
        {
            directCharacter -= 90;
            character.GetComponent<Transform>().rotation = Quaternion.Euler(0, directCharacter, 0);
        }
        if (other.tag == "localEmpty_r")
        {
            directCharacter += 90;
            character.GetComponent<Transform>().rotation = Quaternion.Euler(0, directCharacter, 0);
        }
        if (other.tag == "Respawn")
        {
            DoInit(other);
            DestroyObjectDelayed(other.GetComponent<Transform>().parent.gameObject);
            RandomPlacedPrefabs(other);
        }

    }

    private void RandomPlacedPrefabs(Collider other)
    {

        while (generationStates.Count != CountGenerations)
        {
            int b = RandomFunc();
            if (b == 0)
            {
                d_right = false;
            }
            else if (b == 1)
            {
                d_left = true;
                d_right = false;
            }
            else if (b == 2)
            {
                d_right = true;
                d_left = false;
            }

            generationStates.Add(b);
        }
        while (generationStates.Count != 0)
        {
            GameObject go = GO;
            DoInit(go);
            switch (generationStates[0])
            {
                case 0:
                    DoCorridor();
                    break;
                case 1:
                    DoDegreeLeft();
                    break;
                case 2:
                    DoDegreeRight();
                    break;
            }
            generationStates.RemoveAt(0);
        }
    }

    private int RandomFunc()
    {
        int randomDirect = UnityEngine.Random.Range(0, 3);
        if (randomDirect == 1 && d_left == true)
        {
            return RandomFunc();
        }

        if (randomDirect == 2 && d_right == true)
        {
            return RandomFunc();
        }
        return randomDirect;
    }

    private void DoCorridor()
    {
        Vector3 localCorRot = localRotate.eulerAngles;

        GO = Instantiate(prefab_corr, worldPosPivor, Quaternion.Euler(localCorRot.x, localCorRot.y + directCorridor, localCorRot.z));
        GO.name = "Corridor";
        d_right = false;
    }

    private void DoDegreeLeft()
    {
        loc_D = new Vector3(loc_D.x, loc_D.y + directLeftDegree, loc_D.z);
        GO = Instantiate(prefab_left, worldPosPivor, Quaternion.Euler(loc_D));
        directRightDegree -= 90;
        directCorridor -= 90;
        directLeftDegree -= 90;
        GO.name = "Degree_left";
        d_left = true;
        d_right = false;
    }

    private void DoDegreeRight()
    {

        loc_D = new Vector3(loc_D.x, loc_D.y + directRightDegree, loc_D.z);
        GO = Instantiate(prefab_right, worldPosPivor, Quaternion.Euler(loc_D));
        directLeftDegree += 90;
        directCorridor += 90;
        directRightDegree += 90;
        GO.name = "Degree_right";
        d_right = true;
        d_left = false;
    }

    private void DoInit(Collider other)
    {
        localPivot = other.GetComponent<Transform>().parent.GetChild(1).GetComponent<Transform>();
        worldPosPivor = localPivot.TransformPoint(Vector3.zero);
        localRotate = localPivot.localRotation;
        loc_D = localRotate.eulerAngles;
    }

    private void DoInit(GameObject go)
    {
        localPivot = go.GetComponent<Transform>().GetChild(1).GetComponent<Transform>();
        worldPosPivor = localPivot.TransformPoint(Vector3.zero);
        localRotate = localPivot.localRotation;
        loc_D = localRotate.eulerAngles;
        go.GetComponent<Transform>().GetChild(0).tag = "TriggerDelete";
    }

    private void DeleteBefore(GameObject del)
    {
        Destroy(del, 5);
    }

    private void Init()
    {
        directCorridor = 0;
        directLeftDegree = 0;
        directRightDegree = 0;
        directCharacter = 90;
        generationStates = new List<int>();
    }

    private void DestroyObjectDelayed(GameObject gameObject)
    {
        Destroy(gameObject, TimerDelete);
    }

    private void CreateFirstPref()
    {
        GO = Instantiate(prefab_corr, Vector3.zero, new Quaternion());
        GO.GetComponent<Transform>().name = "First_Corridor";
        DestroyObjectDelayed(GO);
    }

    private void CreateCharacter()
    {
        Instantiate(character, new Vector3(0f, 3f, 0f), Quaternion.Euler(0f, 90f, 0));
    }

    private void MoveCharacter()
    {
        character.GetComponent<Transform>().SetPositionAndRotation(new Vector3(0f, 3f, 0f), Quaternion.Euler(0f, 90f, 0));
    }
}
