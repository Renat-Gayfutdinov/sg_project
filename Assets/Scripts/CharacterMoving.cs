﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterMoving : MonoBehaviour
{
    private Collider col;
    public GameObject menuManager;
    private Initialization initialization;
    private bool readyToTurn;
    public Transform map;
    public GameObject musicPlayer;

    delegate void DashState();
    DashState dashstate;

    Vector3 dashStart;
    private Vector3 dashDestination;
    private float startTime;
    private float dashLength;
    private float dashSpeed;

    Vector3 jumpDestination;
 
    float jumpSpeed;
    float jumpLength;

    float delayTime;

    private int trackNumber;

    AudioSource audioSource;
    public AudioClip tapSound;
    public float tapTime;

    float countDownTime;
    bool ready;

    public bool ReadyToTurn
    {
        get
        {
            return readyToTurn;
        }

        set
        {
            readyToTurn = value;
        }
    }

    public Vector3 DashDestination
    {
        get
        {
            return dashDestination;
        }

        set
        {
            dashDestination = value;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        string tag_C = other.GetComponent<Transform>().name;
        if(tag_C == "Col_2_L" || tag_C == "Col_0_R")
        {
            trackNumber = 1;
        }
        if(tag_C == "Col_2_R" || tag_C == "Col_0_L")
        {
            trackNumber = -1;
        }
        if(tag_C== "Col_1" || tag_C == "Col_1")
        {
            trackNumber = 0;
        }

        if(other.tag == "localEmpty_r"){
            col = other;
            dashstate = TurnCharacter_r;
        }
        if(other.tag == "localEmpty_l"){
            col = other;
           dashstate = TurnCharacter_l;
        }

    }

    private void TurnCharacter_r(){
            Vector3 wpos;
            GameObject ot = col.gameObject;
            wpos = ot.transform.GetChild(0).TransformPoint(Vector3.zero);
            initialization.DirectCharacter += 90;

            GetComponent<Transform>().rotation = Quaternion.Euler(0, initialization.DirectCharacter, 0);
            transform.position = new Vector3(wpos.x, transform.position.y, wpos.z);
            dashstate = DashReady;
    }

    private void TurnCharacter_l(){
            Vector3 wpos;
            GameObject ot = col.gameObject;
            wpos = ot.transform.GetChild(0).TransformPoint(Vector3.zero);
            initialization.DirectCharacter -= 90;

            GetComponent<Transform>().rotation = Quaternion.Euler(0, initialization.DirectCharacter, 0);
            transform.position = new Vector3(wpos.x, transform.position.y, wpos.z);
            dashstate = DashReady;
    }

    // Use this for initialization
    void Start()
    {
        initialization = GameObject.FindGameObjectWithTag("Loader").GetComponent<Initialization>();
        //menuManager.GetComponent<MenuManager>().pauseEvent.AddListener(Pause);
        //menuManager.GetComponent<MenuManager>().playEvent.AddListener(Play);
         MoveCharacter();

        musicPlayer.GetComponent<MusicScript>().countDownEvent.AddListener(WaitForCountDownEnd);

        dashstate = DashReady;
        dashSpeed = 50f;
        trackNumber = 0;
        jumpSpeed = 0;
        jumpDestination = transform.position + Vector3.up;
        jumpLength = Vector3.Distance(transform.position, jumpDestination);
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (musicPlayer.GetComponent<MusicScript>().readyToPlay)
        //{
        countDownTime -= Time.deltaTime;
        if (countDownTime <= 0)
        {
            ready = true;
        }
        if (ready)
        {
            dashstate();
        }
        //}
    }

    void Play ()
    {
        print("play");
        dashstate = DashReady;
    }

    void Pause ()
    {
        print("paused");
        dashstate = Idle;
    }

    void Idle ()
    {

    }

    void Dashing()
    {
        float distCovered = (Time.time - startTime) * dashSpeed;
        float fracDash = distCovered / dashLength;
        dashStart += Vector3.up * jumpSpeed;
        transform.position = Vector3.Lerp(dashStart, DashDestination, fracDash);
        if (transform.position == DashDestination)
        {
            jumpSpeed = 0f;
            dashstate = DashReady;
        }
    }

    void Delayed()
    {
        ReadyToTurn = true;

        delayTime -= Time.deltaTime;
        if (delayTime < 0f)
        {
            startTime = Time.time;
            dashstate = Dashing;
        }
    }

    void DashReady()
    {
        ReadyToTurn = false;
        
        if (Input.GetKeyDown(KeyCode.W))
        {
            DashDestination = transform.position + transform.forward * map.lossyScale.z * 24 * 0.25f;
        }
        if (Input.GetKeyDown(KeyCode.A) && trackNumber > -1)
        {
            DashDestination = transform.position + (-transform.right + transform.forward) * map.lossyScale.z * 24 * 0.25f;
            trackNumber--;
        }
        if (Input.GetKeyDown(KeyCode.D) && trackNumber < 1)
        {
            DashDestination = transform.position + (transform.right + transform.forward) * map.lossyScale.z * 24 * 0.25f;
            trackNumber++;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpSpeed = 0.25f;
            DashDestination = transform.position + transform.forward * map.lossyScale.z * 24 * 0.25f;
        }

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Space))
        {
            //audioSource.PlayOneShot(tapSound);
            delayTime = musicPlayer.GetComponent<MusicScript>().secPerBeat / 4f;
            dashStart = transform.position;
            dashLength = Vector3.Distance(transform.position, DashDestination);
            dashstate = Delayed;
        }
    }

    void WaitForCountDownEnd()
    {
        countDownTime = 2;
        ready = false;
    }

    private void MoveCharacter()
    {
        GetComponent<Transform>().SetPositionAndRotation(new Vector3(0f, 1.8f, 0f), Quaternion.Euler(0f, 90f, 0));
    }
}