using UnityEngine;

public class Bullet:MonoBehaviour{

    private void OnCollisionEnter(Collision col){
        foreach(ContactPoint contact in col.contacts){
            if(contact.otherCollider.tag == "Animal_obst"){
                Destroy(contact.otherCollider.gameObject);
            }
        }
    }
}