﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightningSystemScript : MonoBehaviour {

    public GameObject musicPlayer;
    public GameObject character;

	// Use this for initialization
	void Start () {
        musicPlayer = GameObject.FindGameObjectWithTag("Music_player");
        character = GameObject.FindGameObjectWithTag("Player");
        musicPlayer.GetComponent<MusicScript>().beatEvent.AddListener(TurnOnLight);
    }
	
	// Update is called once per frame
	void Update () {

        if (musicPlayer.GetComponent<AudioSource>().isPlaying)
        {
            foreach (Transform child in transform)
            {
                Color color = child.gameObject.GetComponent<SpriteRenderer>().color;

                if (character.GetComponent<TimingManager>().currentTiming)
                {
                    color.r = 0;
                    color.g = 255;
                    color.b = 0;
                }
                else
                {
                    color.r = 255;
                    color.g = 0;
                    color.b = 0;
                }

                if (color.a > 0)
                {
                    color.a -= 0.025f;
                }

                child.GetComponent<SpriteRenderer>().color = color;
            }
        }
    }

    void TurnOnLight()
    {
        foreach (Transform child in transform)
        {
            Color color = child.gameObject.GetComponent<SpriteRenderer>().color;
            color.a = 1f;
            child.GetComponent<SpriteRenderer>().color = color;

        }
    }
}
