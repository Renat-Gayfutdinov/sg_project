﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {

    public Button play;
    public Button chooseSong;
    public Button exit;

    // Use this for initialization
    void Start () {
        play.onClick.AddListener(Play);
        chooseSong.onClick.AddListener(ChooseSong);
        exit.onClick.AddListener(Exit);
    }
	
    void Play()
    {
        SceneManager.LoadScene("LoadingScene", LoadSceneMode.Single);
    }

    void Exit()
    {
        Application.Quit();
    }

    void ChooseSong()
    {
        
    }

	// Update is called once per frame
	void Update () {
		
	}
}
